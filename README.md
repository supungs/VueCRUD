## VueCRUD

VueCRUD is CRUD interface generator for VueJS. You can define a data model using json and generate the javascript code for the vue app and components.

Checkout in to localhost and visit with a bbrowser.

### Data Model
* name : Singular and plural names of data type in lowercase. This is used for variable names.
* header : Singular and plural names of data type used in UI headers, buttons etc.
* app_props : external data arrays needed for the app. Assumed to be passed from php json_encode
* table_fields : Columns in the table that list the data
* table_refs : External data passed from app to the table comonent
* add_fields : Fields in the add data form. field=db dable column name, name=displayed name in labels, type=text or list(dropdown), ref=if type is list, ref should be an array name with options 
* api : Api urls for put, delete and get
* paginate : Need pagination or not
* per_page : records per page if pagination is used